/* Credits: 
I've written this based on rclc's source code (using rclc_lib): https://github.com/VladimirMarkelov/rclc
And on the algorithm implemented in this video: https://www.youtube.com/watch?v=xDrAncijBq4
*/
use rcalc_lib::parse;
use regex::Regex;
#[macro_use]
extern crate text_io;
fn main(){
    println!("
    I will take any function in terms of z and c, and render its fractal.
    ");
    loop { //loop to avoid recursion into main()
        println!("Do you understand complex numbers? [y/n]");
        let understanding: String = read!();
        if understanding == "y" {
            break;
        }
        if understanding == "n" {
            println!("
                Complex numbers are a way of writing (x, y) co-ordinates, in a way that allows for more versatility.
                Where the x axis would usually be, the real number line is, and the y axis is replaced by the imaginaries
                This means that coordinates are represented as a real number + an imaginary number. For example:
                (2, 4) in normal coordinates becomes: 2+4i.
                See this video for more info: https://www.youtube.com/watch?v=5PcpBw5Hbwo
            ");
            break;
        }
        else {
            println!("Type either y or n");
        }
    }
    input();
}

 
fn input() { //TODO: Support user defined constants
    let mut min_max: Vec<f64> = vec![];
    let mut iters: usize = 0;
    let mut temp: String;
    println!("
    Type the minimum and maximum values you want to process,
    in the order real minimum, real max, imaginary min, im max.
    ");
    while iters < 5 {
        temp = read!();
        if temp.parse::<f64>().is_ok(){ 
            min_max.push(temp.parse().unwrap());
            iters += 1; 
        }
        if temp.parse::<f64>().is_err(){
            println!("Please type a number.");
        }
        //make sure min values are lower than max.
        if iters == 4 && min_max[0] < min_max[1] && min_max[2] < min_max[3]{
            iters += 1;
        } else if iters == 4 && min_max[0] > min_max[1] {
            println!("You've typed a minimum that's higher than a maximum.");
            iters = 0;
        } else if iters == 4 && min_max[2] > min_max[3] {
            println!("You've typed a minimum that's higher than a maximum.");
            iters = 2
        } 
    }
    println!("Input your function(must contain z and c at least once)");
    let expr = loop {
        let mut expr: String = read!();
        expr = expr.trim().to_lowercase();
        let equationre= Regex::new(r".*(z)+.*(c)+|.*(c)+.*(z)").unwrap(); //probably horrible regex: sorry 
        if equationre.is_match(&expr){
            break(expr);
        } else {
            println!("Please input a function that contains z and c.");
        }
    };
    lines_cols(400, 80, min_max, 2000, expr);
}

fn lines_cols(re_width: usize, im_height: usize, min_max: Vec<f64>, iters: usize, expr: String) {
    let re_min = min_max[0];
    let re_max = min_max[1];
    let im_min = min_max[2];
    let im_max = min_max[3];
    let mut rowvec = Vec::<Vec<usize>>::with_capacity(re_width); 
    for im in 0..im_height {
        let im_: f64 = im as f64;
        let im_height_= im_height as f64;
        let mut colvec = Vec::<usize>::with_capacity(im_height);
        for re in 0..re_width {
            let re_: f64 = re as f64;
            let re_width_: f64 = re_width as f64;
            let cre = re_min + (re_max - re_min) * (re_/re_width_);
            let cim = im_min + (im_max - im_min) * (im_/im_height_);
            let stopped_at = calculate_at_point(&expr, iters, cre, cim);
            colvec.push(stopped_at);
        }
        rowvec.push(colvec);
    }
    render(rowvec);
}

fn calculate_at_point(expr: &str, iters: usize, cre: f64, cim: f64) -> usize {
    let mut state = parse::CalcState::new(); 
    let zstr = String::from("0+0i"); 
    let cstr = format!("{}+{}*i", cre, cim);//convert c into complex number form (real+imaginary)
    let istr = String::from("sqrt(-1)");
    let i = parse::eval(&istr, &mut state).expect("Failed to define i");
    state.add_variable("i", i);
    let z = parse::eval(&zstr, &mut state).expect("Failed to define z");
    let c = parse::eval(&cstr, &mut state).expect("Failed to define c"); 
    state.add_variable("z", z);
    state.add_variable("c", c);
    for i in 0..iters {
        match parse::eval(expr, &mut state){
            Ok(v) => {
                state.add_variable("z", v);
            }
            Err(e) => {
                println!("Error in function evaluation: {}",e);
                std::process::exit(1); 
            }
        };
        let z_re: f64 = state.variable("z").unwrap().re().unwrap().to_string().parse().unwrap();
        let z_im: f64 = state.variable("z").unwrap().im().unwrap().to_string().parse().unwrap();
        if z_re > 2.0 || z_im > 2.0 {
            return i;
        }
    } 
    iters
}

fn render(colvec: Vec<Vec<usize>>){ 
    for col in colvec{
        let mut line = String::with_capacity(col.len());
        for row in col{
            let val = match row {
                0..=2 => ' ',
                3..=5 => '.',
                6..=10 => '•',
                11..=20 => '*',
                21..=40 => 'x',
                41..=100 => 'W',
                101..=200 => '%',
                201..=500 => '$',
                501..=1000 => '&',
                _ => '@'
            };
            line.push(val);
        }
    println!("{}", line);
    }
}
