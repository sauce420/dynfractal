# Dynfractal

A program to take any iterative function in terms of z and c, and render its fractal.

## Future plans

This program currently renders the fractal to ASCII on the terminal. <br>
Eventually I'd like to implement a framebuffer, which renders the fractal with a color pallete, and eventually could be interactive (zooming and panning)
